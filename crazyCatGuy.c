///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ ./crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author Adia Cruz <adiacruz@hawaii.edu>
/// @date   16 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );
   int sum = 0;
   for (int i = 1; i <= n; i++){
	  int sum = sum + i;
   } 
  printf(" The sum of the digits from %d to %d is %d\n", 1, n, sum); 

   return 0;
}
